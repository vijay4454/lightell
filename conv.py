#!/usr/bin/env python

import argparse
import math
import numpy as np
import random

INT8_MIN = -128
INT8_MAX = 127

def sat_add(A, B):
  sat_out = min(max(A + B, INT8_MIN), INT8_MAX)
  return sat_out

def sat_mul(A, B):
  sat_out = min(max(A * B, INT8_MIN), INT8_MAX)
  return sat_out

def conv(in_H, in_W, in_chan, out_chan, flt_H, flt_W, stride_H, stride_W,
         rmin, rmax, rseed, in_filename, flt_filename, out_filename):

  in_file = open(in_filename, "w")
  flt_file = open(flt_filename, "w")
  out_file = open(out_filename, "w")

  out_H = int(math.ceil(float(in_H - flt_H + 1) / float(stride_H)));
  out_W = int(math.ceil(float(in_W - flt_W + 1) / float(stride_W)));

  # For simplicity, we assume VALID padding in this computation (as opposed to SAME padding)

  random.seed(rseed)
  inp = np.zeros((in_H, in_W, in_chan), dtype=int)
  for h in range(0, in_H):
    for w in range(0, in_W):
      for c in range(0, in_chan):
        inp[h][w][c] = random.randrange(rmin, rmax, 1)
        in_file.write(str(inp[h][w][c]) + '\n')

  flt = np.zeros((flt_H, flt_W, out_chan, in_chan), dtype=int)
  for h in range(0, flt_H):
    for w in range(0, flt_W):
      for d in range(0, out_chan):
        for c in range(0, in_chan):
          flt[h][w][d][c] = random.randrange(rmin, rmax, 1)
          flt_file.write(str(flt[h][w][d][c]) + '\n')

  i_h = 0
  out = np.zeros((out_H, out_W, out_chan), dtype=int)
  for o_h in range(0, out_H):
    i_w = 0
    for o_w in range(0, out_W):
      for o_c in range(0, out_chan):
        out_val = 0
        for k_h in range(0, flt_H):
          for k_w in range(0, flt_W):
            for i_c in range(0, in_chan):
              out_val = \
                sat_add(out_val, sat_mul(inp[i_h + k_h][i_w + k_w][i_c], flt[k_h][k_w][o_c][i_c]))
        out[o_h][o_w][o_c] = out_val;
        out_file.write(str(out[o_h][o_w][o_c]) + '\n')
      i_w += stride_W
    i_h += stride_H

  in_file.close()
  flt_file.close()
  out_file.close()

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-H', type=int, default=8, help="input_height")
  parser.add_argument('-W', type=int, default=8, help="input_width")
  parser.add_argument('-C', type=int, default=8, help="input_channels")
  parser.add_argument('-D', type=int, default=16, help="input_outputs")
  parser.add_argument('-P', type=int, default=3, help="filter_height")
  parser.add_argument('-Q', type=int, default=3, help="filter_width")
  parser.add_argument('-R', type=int, default=1, help="stride_height")
  parser.add_argument('-S', type=int, default=1, help="stride_width")
  parser.add_argument('--rmin', type=int, default=-128, help="rand_min")
  parser.add_argument('--rmax', type=int, default=127, help="rand_max")
  parser.add_argument('--rseed', type=int, default=0, help="rand_seed")
  parser.add_argument('-I', '--in_filename', type=str, help="in_filename")
  parser.add_argument('-F', '--flt_filename', type=str, help="filter_filename")
  parser.add_argument('-O', '--out_filename', type=str, help="output_filename")
  args = parser.parse_args()

  conv(args.H, args.W, args.C, args.D, args.P, args.Q, args.R, args.S,
       args.rmin, args.rmax, args.rseed,
       args.in_filename, args.flt_filename, args.out_filename)

if __name__ == "__main__":
  main()
