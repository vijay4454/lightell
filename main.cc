/////////////////////////////////////////////////////////////////////////////////////////
// File Name:      main.cc
//
// Author:         Vijay Thiruvengadam
// Email:          vijay2155@gmail.com
//
// Description: tf.nn.conv2d Implementation (see task-details.md for detaiils)
//
// Sources: Lightelligence
////////////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

using namespace std;

/**
 * @brief check if filename is passed, and check if file exists
 *
 * @param filename pointer to filename string
 *
 * @param file file to be checked
 *
 * @return type file type (for printing purposes)
 */
int check_file_validity(char* filename, ifstream& file, string type) {
  if (filename == NULL) {
    cerr << type << " filename is required" << endl;
    return 1;
  }
  file.open(filename);
  if (!file.is_open()) {
    cerr << type << " file is NOT found. Please try again.\n";
    return 1;
  }
  return 0;
}

/**
 * @brief read file contents and populate array
 *
 * @param pointer to num elements in array
 *
 * @param in_filename input filename
 *
 * @return output array
 */
int8_t* read_input(int* nelem, ifstream& in_file) {
  string line;
  *nelem = 0;
  while (getline(in_file, line))
    (*nelem)++;
  in_file.clear();
  in_file.seekg(0, ios::beg);

  int8_t* arr = (int8_t *)malloc(*nelem * sizeof(*arr));
  int index = 0;
  while (getline(in_file, line)) {
    arr[index] = stoi(line);
    index++;
  }

  return arr;
}

/**
 * @brief saturated addition function
 *
 * @param A int8 A value
 *
 * @param B int8 B value
 *
 * @return int8 output value after addition (saturated, if needed)
 */
int8_t sat_add(int8_t A, int8_t B) {
  int16_t out_16 = (int16_t)A + (int16_t)B;
  return (int8_t)min(max(out_16, (int16_t)INT8_MIN), (int16_t)INT8_MAX);
}

/**
 * @brief saturated multiplication function
 *
 * @param A int8 A value
 *
 * @param B int8 B value
 *
 * @return int8 output value after multiplication (saturated, if needed)
 */
int8_t sat_mul(int8_t A, int8_t B) {
  int16_t out_16 = (int16_t)A * (int16_t)B;
  return (int8_t)min(max(out_16, (int16_t)INT8_MIN), (int16_t)INT8_MAX);
}

/**
 * @brief matrix multiply function
 *
 * @param out pointer output matrix (P x N)
 *
 * @param A pointer to input matrix A (N x P)
 *
 * @param B pointer to input matrix B (P x P)
 *
 * @return none
 */
void mult(int8_t* out, int8_t** A, int8_t** B, int N, int P) {
  // Multiplication and additions might lead to overflow if it exceeds
  // signed 8-bit range. Right way to solve the problem is to store intermediate
  // outputs in large bit width (say 32-bit) and when the output value
  // has been fully computed/accumulated, requantize it to 8-bit range.
  // Currently, we do not do requantization. We instead saturate to max/min
  // in int8 range if there is overflow

  // An incorrect way of handling this (apart from requantization, saturation),
  // would be to just let the overflow happen without saturation. This would
  // give completely wrong results. Saturation is a more reasonable approach and
  // requantization is the best approach
  memset(out, 0, P * N);
  for (int n = 0; n < N; n++) {
    for (int p1 = 0; p1 < P; p1++) {
      int8_t* out_addr = out + p1 * N + n;
      for (int p2 = 0; p2 < P; p2++) {
        *out_addr = sat_add(*out_addr, sat_mul(A[n][p2], B[p1][p2]));
      }
    }
  }
}

/**
 * @brief conv2d function
 *
 * @param out_filename name of output file to write output into
 *
 * @param in input array
 *
 * @param nelem_in input array elements
 *
 * @param filter filter array
 *
 * @param nelem_flt filter array elements
 *
 * @param in_H input height
 *
 * @param in_W input width
 *
 * @param in_chan input channels
 *
 * @param out_chan output channels
 *
 * @param flt_H filter height
 *
 * @param flt_W filter width
 *
 * @param stride_H stride in height dimension
 *
 * @param stride_W stride in width dimension
 *
 * @param P size of vector for which hardware-in-question is designed
 *
 * @return Return value indicating error status (0 - no error, 1 - error)
 */
int conv2d(char* out_filename, int8_t* in, int nelem_in, int8_t* flt, int nelem_flt,
           int in_H, int in_W, int in_chan, int out_chan,
           int flt_H, int flt_W, int stride_H, int stride_W, int P) {

  int8_t (*input)[in_W][in_chan] = (int8_t (*)[in_W][in_chan])in;
  int8_t (*filter)[flt_W][out_chan][in_chan] = (int8_t (*)[flt_W][out_chan][in_chan])flt;

  // For simplicity, we assume VALID padding in this computation (as opposed to SAME padding)

  // VALID padding means that you never pad the input, and you are allowed to drop the
  // right-most or bottom-most rows of input to result in a legitimate output size
  // For stride = 1, SAME padding means that input and output will have same size
  // in that dimension after padding
  // For stride > 1, SAME padding means that you pad the input to get a
  // legitimate (non-fraction) size of the output in that dimension

  int N = 2; // num vector processed in one instance of mult() function
  if (out_chan % N != 0) {
    cerr << "output channels must be a multiple of 2\n";
    return 1;
  }

  int out_H = int(ceil(float(in_H - flt_H + 1) / float(stride_H)));
  int out_W = int(ceil(float(in_W - flt_W + 1) / float(stride_W)));
  int8_t* out = (int8_t *)calloc(out_H * out_W * out_chan, sizeof(*out));
  int8_t (*output)[out_W][out_chan] = (int8_t (*)[out_W][out_chan])out;

  int8_t** x = (int8_t **)malloc(N * sizeof(*x));
  int8_t** y = (int8_t **)malloc(P * sizeof(*y));
  int8_t* part_out = (int8_t*)malloc(P * N * sizeof(*part_out));

  for (int o_h = 0; o_h < out_H; o_h++) {
    for (int o_w = 0; o_w < out_W; o_w += P) {
      for (int o_c = 0; o_c < out_chan; o_c += N) {

        for (int k_h = 0; k_h < flt_H; k_h++) {
          for (int k_w = 0; k_w < flt_W; k_w++) {

            for (int idx = 0; idx < in_chan / P; idx++) {
              for (int n = 0; n < N; n++) {
                x[n] = &filter[k_h][k_w][o_c + n][idx * P];
              }
              for (int p = 0; p < P; p++) {
                y[p] = &input[o_h * stride_H + k_h][o_w * stride_W + k_w + p][idx * P];
              }

              mult(part_out, x, y, N, P);

              // Since we are doing 8-bit saturation math here, the ordering
              // of the addition of the part_out will lead to different results
              // For example, the "mult" function does not use any of the previously
              // generated partial sums, and produces a new part_out each time. We then
              // add the part_out to accumulated values in output. This will produce
              // different results than the case when we add the product of individual
              // elements to a running partial sum (like what the simpler non-mult
              // version of the code is doing - see commented code below)
              for (int p = 0; p < P; p++) {
                for (int n = 0; n < N; n++) {
                  output[o_h][o_w + p][o_c + n] =
                    sat_add(output[o_h][o_w + p][o_c + n], *(part_out + p * N + n));
                }
              }
            }

          }
        }
      }
    }
  }

  // Find the Straight-forward method below, without use of "mult" function
  /*
  for (int o_h = 0; o_h < out_H; o_h++) {
    for (int o_w = 0; o_w < out_W; o_w++) {
      for (int o_c = 0; o_c < out_chan; o_c++) {
        int8_t out_val = 0;
        for (int k_h = 0; k_h < flt_H; k_h++) {
          for (int k_w = 0; k_w < flt_W; k_w++) {
            for (int i_c = 0; i_c < in_chan; i_c++) {
              out_val = sat_add(out_val,
                          sat_mul(input[o_h * stride_H + k_h][o_w * stride_W + k_w][i_c],
                                  filter[k_h][k_w][o_c][i_c]));
            }
          }
        }
        output[o_h][o_w][o_c] = out_val;
      }
    }
  }
  */

  ofstream out_file;
  out_file.open(out_filename);
  if (!out_file.is_open()) {
    cerr << "output file is NOT found\n";
    return 1;
  }
  for (int o_h = 0; o_h < out_H; o_h++) {
    for (int o_w = 0; o_w < out_W; o_w++) {
      for (int o_c = 0; o_c < out_chan; o_c++) {
        out_file << to_string(output[o_h][o_w][o_c]) << "\n";
      }
    }
  }
  out_file.close();

  free(part_out);
  free(y);
  free(x);
  free(in);
  free(flt);
  free(out);

  return 0;
}

/**
 * @brief Main function for conv2d function
 *
 * @param argc argument count
 *
 * @param argv argument vector (1D array of strings)
 *
 * @return Return value indicating error status (0 - no error, 1 - error)
 */
int main(int argc, char** argv) {

  int H = -1; // input height
  int W = -1; // input width
  int C = -1; // input channels
  int D = -1; // output channels
  int flt_H = -1; // filter height
  int flt_W = -1; // filter width
  int stride_H = -1; // stride in height dimension
  int stride_W = -1; // stride in width dimension
  char* in_filename = NULL; // filename for file containing input data
  char* flt_filename = NULL; // filename for containing filter data
  char* out_filename = NULL; // filename for file to which output will be written
  int P = -1; // size of vector for which hardware-in-question is designed

  int opt;
  while ((opt = getopt(argc, argv, "H:W:C:D:P:Q:R:S:I:F:O:T:")) != -1) {
    switch (opt) {
    case 'H':
      H = atoi(optarg);
      break;
    case 'W':
      W = atoi(optarg);
      break;
    case 'C':
      C = atoi(optarg);
      break;
    case 'D':
      D = atoi(optarg);
      break;
    case 'P':
      flt_H = atoi(optarg);
      break;
    case 'Q':
      flt_W = atoi(optarg);
      break;
    case 'R':
      stride_H = atoi(optarg);
      break;
    case 'S':
      stride_W = atoi(optarg);
      break;
    case 'T':
      P = atoi(optarg);
      break;
    case 'I':
      in_filename = optarg;
      break;
    case 'F':
      flt_filename = optarg;
      break;
    case 'O':
      out_filename = optarg;
      break;
    default:
      cerr << "Improper arguments";
      return 1;
    }
  }

  for (int i = optind; i < argc; i++)
    cout << "Non option argument: " << argv[i] << endl;

  if (H < 1 || W < 1 || C < 1 || D < 1 || flt_H < 1 || flt_W < 1 || stride_H < 1 || stride_W < 1 || P < 1) {
    cerr << "Invalid input dimension" << endl;
    return 1;
  }

  if (C % P != 0) {
    cerr << "input channels must be a multiple of P (hardware supported vector size)\n";
    return 1;
  }

  ifstream in_file;
  ifstream flt_file;
  ifstream out_file;
  if (check_file_validity(in_filename, in_file, "input"))
    return 1;
  if (check_file_validity(flt_filename, flt_file, "filter"))
    return 1;
  if (out_filename == NULL) {
    cerr << "output filename is required" << endl;
    return 1;
  }

  int nelem_in;
  int8_t* in = read_input(&nelem_in, in_file);
  in_file.close();

  int nelem_flt;
  int8_t* flt = read_input(&nelem_flt, flt_file);
  flt_file.close();

  int ret_val = conv2d(out_filename, in, nelem_in, flt, nelem_flt,
                       H, W, C, D, flt_H, flt_W, stride_H, stride_W, P);

  return ret_val;
}
