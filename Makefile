.PHONY: check clean

CC = g++
CXXFLAGS = -std=c++11
EXE = conv2d

ifdef DEBUG
CXXFLAGS += -g
endif

$(EXE): main.o
	$(CC) -o conv2d main.o $(CXXFLAGS)

main.o: main.cc
	$(CC) -c main.cc $(CXXFLAGS)

# First, run the python version of conv2d to generate the input, filter, and ref output
# Next, run the C++ version (design-under-test)
# Compare the reference against the output from C++ program
check-%: $(EXE)
	./conv.py $(PARAMS_$*) -O out-ref-$*.txt --rmin -5 --rmax 5 --rseed 0
	./$(EXE) $(PARAMS_$*) -T2 -O out-$*.txt
	diff out-$*.txt out-ref-$*.txt

TESTS = test1 test2 test3
SUBCHECKS = $(foreach t, $(TESTS), check-$t)

PARAMS_test1 = -H2 -W2 -C2 -D2 -P1 -Q1 -R1 -S1 -I in-test1.txt -F flt-test1.txt
PARAMS_test2 = -H8 -W8 -C8 -D16 -P1 -Q1 -R1 -S1 -I in-test2.txt -F flt-test2.txt
PARAMS_test3 = -H4 -W8 -C8 -D4 -P1 -Q1 -R1 -S1 -I in-test3.txt -F flt-test3.txt

check: $(SUBCHECKS)

clean:
	rm -rf main.o $(EXE) *.txt
